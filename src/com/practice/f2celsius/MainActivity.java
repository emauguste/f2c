package com.practice.f2celsius;

import java.text.DecimalFormat;

import android.os.Bundle;
import android.app.Activity;
import android.view.View.OnClickListener;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
//import android.support.v4.app.NavUtils;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        //Capturing the button from the layout
        Button button = (Button) findViewById(R.id.convert_button);
        //Register the onClick listener
        button.setOnClickListener(calculate);
    } 

    
    private OnClickListener calculate = new OnClickListener()
    {
    	public void onClick(View v)
    	{   		
    		EditText edittext = (EditText) findViewById(R.id.editFahrenheit);
    		double fahrenheit = Double.parseDouble(edittext.getText().toString());
    		double total = (fahrenheit -32) / 1.8;
    		
    		TextView textview = (TextView) findViewById(R.id.result);
    		
    		//Formatting to double digit precision
    		total = Double.parseDouble(new DecimalFormat("#.##").format(total));
    		textview.setText(Double.toString(total));
	
    	}
    };
   
    
    /* 
     * import java.math.BigDecimal;private static double round(double unrounded, int precision, int roundingMode)
    {
        BigDecimal bd = new BigDecimal(unrounded);
        BigDecimal rounded = bd.setScale(precision, roundingMode);
        return rounded.doubleValue();
    }*/

    
}
